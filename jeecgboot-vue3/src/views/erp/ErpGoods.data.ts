import {BasicColumn} from '/@/components/Table';
import {FormSchema} from '/@/components/Table';
import { rules} from '/@/utils/helper/validator';
import { render } from '/@/utils/common/renderUtils';
//列表数据
export const columns: BasicColumn[] = [
   {
    title: '商品SN',
    align:"center",
    sorter: true,
    dataIndex: 'goodsSn'
   },
   {
    title: '商品名称',
    align:"center",
    dataIndex: 'goodsName'
   },
   {
    title: '规格',
    align:"center",
    dataIndex: 'spec_dictText'
   },
   {
    title: '库存数',
    align:"center",
    dataIndex: 'goodsNumber'
   },
   {
    title: '产品分类',
    align:"center",
    dataIndex: 'categoryName_dictText'
   },
   {
    title: '进货价',
    align:"center",
    dataIndex: 'buyingPrice'
   },
   {
    title: '售货价',
    align:"center",
    dataIndex: 'sellingPrice'
   },
   {
    title: '售价折扣',
    align:"center",
    dataIndex: 'sellingDiscount'
   },
   {
    title: '库存报警数',
    align:"center",
    dataIndex: 'warnNumber'
   },
];
//查询数据
export const searchFormSchema: FormSchema[] = [
	{
      label: "规格",
      field: 'spec',
      component: 'JDictSelectTag',
      componentProps:{
      },
      colProps: {span: 6},
 	},
	{
      label: "产品分类",
      field: 'categoryName',
      component: 'JDictSelectTag',
      componentProps:{
      },
      colProps: {span: 6},
 	},
];
//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '商品SN',
    field: 'goodsSn',
    component: 'Input',
  },
  {
    label: '商品名称',
    field: 'goodsName',
    component: 'Input',
    dynamicRules: ({model,schema}) => {
          return [
                 { required: true, message: '请输入商品名称!'},
          ];
     },
  },
  {
    label: '规格',
    field: 'spec',
    component: 'JDictSelectTag',
    componentProps:{
        dictCode:""
     },
  },
  {
    label: '库存数',
    field: 'goodsNumber',
    component: 'InputNumber',
    dynamicRules: ({model,schema}) => {
          return [
                 { required: true, message: '请输入库存数!'},
          ];
     },
  },
  {
    label: '产品分类',
    field: 'categoryName',
    component: 'JDictSelectTag',
    componentProps:{
        dictCode:""
     },
  },
  {
    label: '进货价',
    field: 'buyingPrice',
    component: 'InputNumber',
    dynamicRules: ({model,schema}) => {
          return [
                 { required: true, message: '请输入进货价!'},
          ];
     },
  },
  {
    label: '售货价',
    field: 'sellingPrice',
    component: 'InputNumber',
    dynamicRules: ({model,schema}) => {
          return [
                 { required: true, message: '请输入售货价!'},
          ];
     },
  },
  {
    label: '售价折扣',
    field: 'sellingDiscount',
    defaultValue: 95,
    component: 'InputNumber',
    dynamicRules: ({model,schema}) => {
          return [
                 { required: true, message: '请输入售价折扣!'},
          ];
     },
  },
  {
    label: '库存报警数',
    field: 'warnNumber',
    component: 'InputNumber',
    dynamicRules: ({model,schema}) => {
          return [
                 { required: true, message: '请输入库存报警数!'},
          ];
     },
  },
  {
    label: '供货人名称',
    field: 'providerName',
    component: 'Input',
  },
  {
    label: '商品图',
    field: 'goodsImg',
     component: 'JImageUpload',
     componentProps:{
      },
  },
	// TODO 主键隐藏字段，目前写死为ID
	{
	  label: '',
	  field: 'id',
	  component: 'Input',
	  show: false
	},
];



/**
* 流程表单调用这个方法获取formSchema
* @param param
*/
export function getBpmFormSchema(_formData): FormSchema[]{
  // 默认和原始表单保持一致 如果流程中配置了权限数据，这里需要单独处理formSchema
  return formSchema;
}