# jeecgboot-erp

#### 介绍
基于jeecgboot生成的小ERP系统。

<img src="静态图片/1.png">

<img src="静态图片/2.png">

<img src="静态图片/3.png">

<img src="静态图片/5.png">

<img src="静态图片/7.png">

<img src="静态图片/9.png">

#### 软件架构
基于jeecgboot和jeecgboot-uniapp的一个小的进销存管理软件，目前支持库存商品查看，入库和出库，并更改库存。

简简单单的功能。



#### 安装教程

1.  下载或者clone代码，建立数据库，导入示例数据。
2.  Java端安装和运行参考：https://github.com/jeecgboot/jeecg-boot
3.  jeecgboot-uniapp参考：https://github.com/jeecgboot/jeecg-uniapp
4.  如果要从新生成代码，需要用到 jeecgboot-vue3，参考：https://github.com/jeecgboot/jeecgboot-vue3

#### 使用说明

1.  该代码基于jeecgboot生成并做了简单修改，如果想要jeecgboot更多强大功能请支持jeecgboot商业版。
2.  本程序仅仅是个人使用，仅仅完成基础功能，仅具有学习价值，开发和配置过程有计划做个视频出来，一来是备忘，二来是慢慢开启学习之旅。
3.  视频地址：暂无

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
