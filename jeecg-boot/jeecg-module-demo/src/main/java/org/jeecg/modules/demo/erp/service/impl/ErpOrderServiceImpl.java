package org.jeecg.modules.demo.erp.service.impl;

import org.jeecg.modules.demo.erp.entity.ErpGoods;
import org.jeecg.modules.demo.erp.entity.ErpOrder;
import org.jeecg.modules.demo.erp.entity.ErpOrderItems;
import org.jeecg.modules.demo.erp.mapper.ErpGoodsMapper;
import org.jeecg.modules.demo.erp.mapper.ErpOrderItemsMapper;
import org.jeecg.modules.demo.erp.mapper.ErpOrderMapper;
import org.jeecg.modules.demo.erp.service.IErpOrderService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Collection;
import java.util.Map;

/**
 * @Description: ERP_出入仓
 * @Author: jeecg-boot
 * @Date: 2022-12-04
 * @Version: V1.0
 */
@Service
public class ErpOrderServiceImpl extends ServiceImpl<ErpOrderMapper, ErpOrder> implements IErpOrderService {

    @Autowired
    private ErpOrderMapper erpOrderMapper;
    @Autowired
    private ErpOrderItemsMapper erpOrderItemsMapper;
    @Autowired
    private ErpGoodsMapper erpGoodsMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveMain(ErpOrder erpOrder, List<ErpOrderItems> erpOrderItemsList) {
        erpOrderMapper.insert(erpOrder);
        if (erpOrderItemsList != null && erpOrderItemsList.size() > 0) {
            for (ErpOrderItems entity : erpOrderItemsList) {
                //外键设置
                entity.setOrderId(erpOrder.getId());
                erpOrderItemsMapper.insert(entity);

                // 入库减少库存
                ErpGoods erpGoods = erpGoodsMapper.selectById(entity.getGoodsId());
//                Synstem.out.println("insert before:" + erpGoods.toString());
                ErpGoods erpGoodsUpdate = new ErpGoods();
                erpGoodsUpdate.setId(entity.getGoodsId());
                erpGoodsUpdate.setGoodsNumber(erpGoods.getGoodsNumber() - entity.getGoodsNumber());
                erpGoodsMapper.updateById(erpGoodsUpdate);
            }
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateMain(ErpOrder erpOrder, List<ErpOrderItems> erpOrderItemsList) {
        erpOrderMapper.updateById(erpOrder);

        Map<String, Integer> goodsCntMap = new HashMap<>();

        List<ErpOrderItems> erpOrderItemsList1 = erpOrderItemsMapper.selectByMainId(erpOrder.getId());
        erpOrderItemsList1.forEach(item -> {
            goodsCntMap.put(item.getGoodsId(), -item.getGoodsNumber());
        });

        System.out.println("更新前的数组：" + goodsCntMap.toString());

        //1.先删除子表数据
        erpOrderItemsMapper.deleteByMainId(erpOrder.getId());

        //2.子表数据重新插入
        if (erpOrderItemsList != null && erpOrderItemsList.size() > 0) {
            for (ErpOrderItems entity : erpOrderItemsList) {
                //外键设置
                entity.setOrderId(erpOrder.getId());
                erpOrderItemsMapper.insert(entity);

                int mapGoodsCnt = goodsCntMap.get(entity.getGoodsId());
                System.out.println("MAP中数量：" + mapGoodsCnt);
                goodsCntMap.put(entity.getGoodsId(), mapGoodsCnt + entity.getGoodsNumber());
            }
        }

        System.out.println("更新后的数组：" + goodsCntMap.toString());

        // 更新库存
        goodsCntMap.forEach((goodsId, cnt) -> {
            ErpGoods erpGoods = erpGoodsMapper.selectById(goodsId);
            ErpGoods erpGoodsUpdate = new ErpGoods();
            erpGoodsUpdate.setId(goodsId);
            erpGoodsUpdate.setGoodsNumber(erpGoods.getGoodsNumber() - cnt);
            erpGoodsMapper.updateById(erpGoodsUpdate);
        });
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delMain(String id) {
        erpOrderItemsMapper.deleteByMainId(id);
        erpOrderMapper.deleteById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delBatchMain(Collection<? extends Serializable> idList) {
        for (Serializable id : idList) {
            erpOrderItemsMapper.deleteByMainId(id.toString());
            erpOrderMapper.deleteById(id);
        }
    }

}
