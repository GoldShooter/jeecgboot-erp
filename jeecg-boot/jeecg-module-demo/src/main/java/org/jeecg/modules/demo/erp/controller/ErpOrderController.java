package org.jeecg.modules.demo.erp.controller;

import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.vo.LoginUser;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.demo.erp.entity.ErpOrderItems;
import org.jeecg.modules.demo.erp.entity.ErpOrder;
import org.jeecg.modules.demo.erp.vo.ErpOrderPage;
import org.jeecg.modules.demo.erp.service.IErpOrderService;
import org.jeecg.modules.demo.erp.service.IErpOrderItemsService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.apache.shiro.authz.annotation.RequiresPermissions;


 /**
 * @Description: ERP_出入仓
 * @Author: jeecg-boot
 * @Date:   2022-12-04
 * @Version: V1.0
 */
@Api(tags="ERP_出入仓")
@RestController
@RequestMapping("/erp/erpOrder")
@Slf4j
public class ErpOrderController {
	@Autowired
	private IErpOrderService erpOrderService;
	@Autowired
	private IErpOrderItemsService erpOrderItemsService;
	
	/**
	 * 分页列表查询
	 *
	 * @param erpOrder
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "ERP_出入仓-分页列表查询")
	@ApiOperation(value="ERP_出入仓-分页列表查询", notes="ERP_出入仓-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<ErpOrder>> queryPageList(ErpOrder erpOrder,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<ErpOrder> queryWrapper = QueryGenerator.initQueryWrapper(erpOrder, req.getParameterMap());
		Page<ErpOrder> page = new Page<ErpOrder>(pageNo, pageSize);
		IPage<ErpOrder> pageList = erpOrderService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param erpOrderPage
	 * @return
	 */
	@AutoLog(value = "ERP_出入仓-添加")
	@ApiOperation(value="ERP_出入仓-添加", notes="ERP_出入仓-添加")
    //@RequiresPermissions("erp:erp_order:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody ErpOrderPage erpOrderPage) {
		ErpOrder erpOrder = new ErpOrder();
		BeanUtils.copyProperties(erpOrderPage, erpOrder);
		erpOrderService.saveMain(erpOrder, erpOrderPage.getErpOrderItemsList());
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param erpOrderPage
	 * @return
	 */
	@AutoLog(value = "ERP_出入仓-编辑")
	@ApiOperation(value="ERP_出入仓-编辑", notes="ERP_出入仓-编辑")
    //@RequiresPermissions("erp:erp_order:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody ErpOrderPage erpOrderPage) {
		ErpOrder erpOrder = new ErpOrder();
		BeanUtils.copyProperties(erpOrderPage, erpOrder);
		ErpOrder erpOrderEntity = erpOrderService.getById(erpOrder.getId());
		if(erpOrderEntity==null) {
			return Result.error("未找到对应数据");
		}
		erpOrderService.updateMain(erpOrder, erpOrderPage.getErpOrderItemsList());
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "ERP_出入仓-通过id删除")
	@ApiOperation(value="ERP_出入仓-通过id删除", notes="ERP_出入仓-通过id删除")
    //@RequiresPermissions("erp:erp_order:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		erpOrderService.delMain(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "ERP_出入仓-批量删除")
	@ApiOperation(value="ERP_出入仓-批量删除", notes="ERP_出入仓-批量删除")
    //@RequiresPermissions("erp:erp_order:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.erpOrderService.delBatchMain(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "ERP_出入仓-通过id查询")
	@ApiOperation(value="ERP_出入仓-通过id查询", notes="ERP_出入仓-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<ErpOrder> queryById(@RequestParam(name="id",required=true) String id) {
		ErpOrder erpOrder = erpOrderService.getById(id);
		if(erpOrder==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(erpOrder);

	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "ERP_出入仓明细通过主表ID查询")
	@ApiOperation(value="ERP_出入仓明细主表ID查询", notes="ERP_出入仓明细-通主表ID查询")
	@GetMapping(value = "/queryErpOrderItemsByMainId")
	public Result<List<ErpOrderItems>> queryErpOrderItemsListByMainId(@RequestParam(name="id",required=true) String id) {
		List<ErpOrderItems> erpOrderItemsList = erpOrderItemsService.selectByMainId(id);
		return Result.OK(erpOrderItemsList);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param erpOrder
    */
    //@RequiresPermissions("erp:erp_order:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ErpOrder erpOrder) {
      // Step.1 组装查询条件查询数据
      QueryWrapper<ErpOrder> queryWrapper = QueryGenerator.initQueryWrapper(erpOrder, request.getParameterMap());
      LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

      //配置选中数据查询条件
      String selections = request.getParameter("selections");
      if(oConvertUtils.isNotEmpty(selections)) {
         List<String> selectionList = Arrays.asList(selections.split(","));
         queryWrapper.in("id",selectionList);
      }
      //Step.2 获取导出数据
      List<ErpOrder> erpOrderList = erpOrderService.list(queryWrapper);

      // Step.3 组装pageList
      List<ErpOrderPage> pageList = new ArrayList<ErpOrderPage>();
      for (ErpOrder main : erpOrderList) {
          ErpOrderPage vo = new ErpOrderPage();
          BeanUtils.copyProperties(main, vo);
          List<ErpOrderItems> erpOrderItemsList = erpOrderItemsService.selectByMainId(main.getId());
          vo.setErpOrderItemsList(erpOrderItemsList);
          pageList.add(vo);
      }

      // Step.4 AutoPoi 导出Excel
      ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
      mv.addObject(NormalExcelConstants.FILE_NAME, "ERP_出入仓列表");
      mv.addObject(NormalExcelConstants.CLASS, ErpOrderPage.class);
      mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("ERP_出入仓数据", "导出人:"+sysUser.getRealname(), "ERP_出入仓"));
      mv.addObject(NormalExcelConstants.DATA_LIST, pageList);
      return mv;
    }

    /**
    * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    //@RequiresPermissions("erp:erp_order:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
      Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
      for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
          // 获取上传文件对象
          MultipartFile file = entity.getValue();
          ImportParams params = new ImportParams();
          params.setTitleRows(2);
          params.setHeadRows(1);
          params.setNeedSave(true);
          try {
              List<ErpOrderPage> list = ExcelImportUtil.importExcel(file.getInputStream(), ErpOrderPage.class, params);
              for (ErpOrderPage page : list) {
                  ErpOrder po = new ErpOrder();
                  BeanUtils.copyProperties(page, po);
                  erpOrderService.saveMain(po, page.getErpOrderItemsList());
              }
              return Result.OK("文件导入成功！数据行数:" + list.size());
          } catch (Exception e) {
              log.error(e.getMessage(),e);
              return Result.error("文件导入失败:"+e.getMessage());
          } finally {
              try {
                  file.getInputStream().close();
              } catch (IOException e) {
                  e.printStackTrace();
              }
          }
      }
      return Result.OK("文件导入失败！");
    }

}
