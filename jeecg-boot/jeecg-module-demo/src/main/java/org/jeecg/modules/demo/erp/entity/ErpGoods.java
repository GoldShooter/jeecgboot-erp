package org.jeecg.modules.demo.erp.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: ERP_库存商品
 * @Author: jeecg-boot
 * @Date:   2022-12-03
 * @Version: V1.0
 */
@Data
@TableName("erp_goods")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="erp_goods对象", description="ERP_库存商品")
public class ErpGoods implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**商品SN*/
	@Excel(name = "商品SN", width = 15)
    @ApiModelProperty(value = "商品SN")
    private java.lang.String goodsSn;
	/**商品名称*/
	@Excel(name = "商品名称", width = 15)
    @ApiModelProperty(value = "商品名称")
    private java.lang.String goodsName;
	/**规格*/
	@Excel(name = "规格", width = 15)
    @ApiModelProperty(value = "规格")
    private java.lang.String spec;
	/**库存数*/
	@Excel(name = "库存数", width = 15)
    @ApiModelProperty(value = "库存数")
    private java.lang.Integer goodsNumber;
	/**产品分类*/
	@Excel(name = "产品分类", width = 15)
    @ApiModelProperty(value = "产品分类")
    private java.lang.String categoryName;
	/**进货价*/
	@Excel(name = "进货价", width = 15)
    @ApiModelProperty(value = "进货价")
    private java.math.BigDecimal buyingPrice;
	/**售货价*/
	@Excel(name = "售货价", width = 15)
    @ApiModelProperty(value = "售货价")
    private java.math.BigDecimal sellingPrice;
	/**售价折扣*/
	@Excel(name = "售价折扣", width = 15)
    @ApiModelProperty(value = "售价折扣")
    private java.lang.Integer sellingDiscount;
	/**库存报警数*/
	@Excel(name = "库存报警数", width = 15)
    @ApiModelProperty(value = "库存报警数")
    private java.lang.Integer warnNumber;
	/**供货人名称*/
	@Excel(name = "供货人名称", width = 15)
    @ApiModelProperty(value = "供货人名称")
    private java.lang.String providerName;
	/**商品图*/
	@Excel(name = "商品图", width = 15)
    @ApiModelProperty(value = "商品图")
    private java.lang.String goodsImg;
}
