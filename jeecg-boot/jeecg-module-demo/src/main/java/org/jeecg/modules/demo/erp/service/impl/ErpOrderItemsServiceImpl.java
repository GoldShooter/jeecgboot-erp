package org.jeecg.modules.demo.erp.service.impl;

import org.jeecg.modules.demo.erp.entity.ErpOrderItems;
import org.jeecg.modules.demo.erp.mapper.ErpOrderItemsMapper;
import org.jeecg.modules.demo.erp.service.IErpOrderItemsService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: ERP_出入仓明细
 * @Author: jeecg-boot
 * @Date:   2022-12-04
 * @Version: V1.0
 */
@Service
public class ErpOrderItemsServiceImpl extends ServiceImpl<ErpOrderItemsMapper, ErpOrderItems> implements IErpOrderItemsService {
	
	@Autowired
	private ErpOrderItemsMapper erpOrderItemsMapper;
	
	@Override
	public List<ErpOrderItems> selectByMainId(String mainId) {
		return erpOrderItemsMapper.selectByMainId(mainId);
	}
}
