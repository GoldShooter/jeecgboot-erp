package org.jeecg.modules.demo.erp.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.demo.erp.entity.ErpGoods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: ERP_库存商品
 * @Author: jeecg-boot
 * @Date:   2022-12-03
 * @Version: V1.0
 */
public interface ErpGoodsMapper extends BaseMapper<ErpGoods> {

}
