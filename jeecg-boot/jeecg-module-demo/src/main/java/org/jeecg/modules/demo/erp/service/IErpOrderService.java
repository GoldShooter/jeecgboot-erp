package org.jeecg.modules.demo.erp.service;

import org.jeecg.modules.demo.erp.entity.ErpOrderItems;
import org.jeecg.modules.demo.erp.entity.ErpOrder;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: ERP_出入仓
 * @Author: jeecg-boot
 * @Date:   2022-12-04
 * @Version: V1.0
 */
public interface IErpOrderService extends IService<ErpOrder> {

	/**
	 * 添加一对多
	 *
	 * @param erpOrder
	 * @param erpOrderItemsList
	 */
	public void saveMain(ErpOrder erpOrder,List<ErpOrderItems> erpOrderItemsList) ;
	
	/**
	 * 修改一对多
	 *
   * @param erpOrder
   * @param erpOrderItemsList
	 */
	public void updateMain(ErpOrder erpOrder,List<ErpOrderItems> erpOrderItemsList);
	
	/**
	 * 删除一对多
	 *
	 * @param id
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 *
	 * @param idList
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);
	
}
