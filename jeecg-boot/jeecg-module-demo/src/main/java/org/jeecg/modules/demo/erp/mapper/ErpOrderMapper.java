package org.jeecg.modules.demo.erp.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.demo.erp.entity.ErpOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: ERP_出入仓
 * @Author: jeecg-boot
 * @Date:   2022-12-04
 * @Version: V1.0
 */
public interface ErpOrderMapper extends BaseMapper<ErpOrder> {

}
