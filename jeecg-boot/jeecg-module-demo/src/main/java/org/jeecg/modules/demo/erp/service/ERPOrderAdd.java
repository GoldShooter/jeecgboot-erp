package org.jeecg.modules.demo.erp.service;

import com.alibaba.fastjson.JSONObject;
import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.modules.online.cgform.enhance.CgformEnhanceJavaInter;
import org.jeecg.modules.online.cgform.enhance.CgformEnhanceJavaListInter;
import org.jeecg.modules.online.cgform.mapper.OnlCgformFieldMapper;
import org.jeecg.modules.online.config.exception.BusinessException;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component("ERPOrderAdd")
public class ERPOrderAdd implements CgformEnhanceJavaInter {
    @Override
    public void execute(String tableName, JSONObject jsonObject) throws BusinessException {
        System.out.println("ERPOrderAdd.execute");
        OnlCgformFieldMapper onlCgformFieldMapper = SpringContextUtils.getBean(OnlCgformFieldMapper.class);
        Map<String, Object> params = new HashMap<>();
        params.put("execute_sql_string", "update " + tableName + " set create_by='111' where id=#{id,jdbcType=VARCHAR}");
        params.put("id", jsonObject.get("id"));
        onlCgformFieldMapper.executeUpdatetSQL(params);
    }


//    @Override
//    public void execute(String s, List<Map<String, Object>> list) throws BusinessException {
//        System.out.println("ERPOrderAdd.execute");
//        System.out.println("s = " + s + ", list = " + list);
//    }
}
