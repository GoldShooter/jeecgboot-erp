package org.jeecg.modules.demo.erp.service;

import org.jeecg.modules.demo.erp.entity.ErpGoods;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: ERP_库存商品
 * @Author: jeecg-boot
 * @Date:   2022-12-03
 * @Version: V1.0
 */
public interface IErpGoodsService extends IService<ErpGoods> {

}
