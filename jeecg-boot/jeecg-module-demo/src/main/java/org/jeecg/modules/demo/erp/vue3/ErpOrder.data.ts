import {BasicColumn} from '/@/components/Table';
import {FormSchema} from '/@/components/Table';
import { rules} from '/@/utils/helper/validator';
import { render } from '/@/utils/common/renderUtils';
import {JVxeTypes,JVxeColumn} from '/@/components/jeecg/JVxeTable/types'
//列表数据
export const columns: BasicColumn[] = [
   {
    title: '单据编码',
    align:"center",
    dataIndex: 'orderSn'
   },
   {
    title: '总金额',
    align:"center",
    dataIndex: 'amount'
   },
   {
    title: '商品数量',
    align:"center",
    dataIndex: 'goodsCnt'
   },
   {
    title: '订单类型',
    align:"center",
    dataIndex: 'orderType'
   },
   {
    title: '创建日期',
    align:"center",
    dataIndex: 'createTime'
   },
];
//查询数据
export const searchFormSchema: FormSchema[] = [
	{
      label: "单据编码",
      field: "orderSn",
      component: 'Input',
      colProps: {span: 6},
 	},
];
//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '单据编码',
    field: 'orderSn',
    component: 'Input',
  },
  {
    label: '总金额',
    field: 'amount',
    component: 'InputNumber',
  },
  {
    label: '商品数量',
    field: 'goodsCnt',
    component: 'InputNumber',
  },
  {
    label: '订单类型',
    field: 'orderType',
    component: 'Input',
  },
	// TODO 主键隐藏字段，目前写死为ID
	{
	  label: '',
	  field: 'id',
	  component: 'Input',
	  show: false
	},
];
//子表单数据
//子表表格配置
export const erpOrderItemsColumns: JVxeColumn[] = [
    {
      title: '商品名称',
      key: 'goodsName',
      type: JVxeTypes.popup,
      popupCode:"erp_goods",
      fieldConfig: [
        { source: 'id', target: 'goodsId' },
        { source: 'goods_sn', target: 'goodsSn' },
        { source: 'goods_name', target: 'goodsName' },
        { source: 'spec', target: 'goodsSpec' },
        { source: 'selling_price', target: 'goodsPrice' },
      ],

      width:"200px",
      placeholder: '请输入${title}',
      defaultValue:'',
    },
    {
      title: '规格',
      key: 'goodsSpec',
      type: JVxeTypes.input,
      width:"200px",
      placeholder: '请输入${title}',
      defaultValue:'',
    },
    {
      title: '数量',
      key: 'goodsNumber',
      type: JVxeTypes.inputNumber,
      width:"200px",
      placeholder: '请输入${title}',
      defaultValue:'',
    },
    {
      title: '商品价格',
      key: 'goodsPrice',
      type: JVxeTypes.inputNumber,
      width:"200px",
      placeholder: '请输入${title}',
      defaultValue:'',
    },
    {
      title: '商品总价',
      key: 'goodsAmount',
      type: JVxeTypes.inputNumber,
      width:"200px",
      placeholder: '请输入${title}',
      defaultValue:'',
    },
  ]


/**
* 流程表单调用这个方法获取formSchema
* @param param
*/
export function getBpmFormSchema(_formData): FormSchema[]{
// 默认和原始表单保持一致 如果流程中配置了权限数据，这里需要单独处理formSchema
  return formSchema;
}