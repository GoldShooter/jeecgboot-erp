package org.jeecg.modules.demo.erp.service;

import org.jeecg.modules.demo.erp.entity.ErpOrderItems;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: ERP_出入仓明细
 * @Author: jeecg-boot
 * @Date:   2022-12-04
 * @Version: V1.0
 */
public interface IErpOrderItemsService extends IService<ErpOrderItems> {

	/**
	 * 通过主表id查询子表数据
	 *
	 * @param mainId 主表id
	 * @return List<ErpOrderItems>
	 */
	public List<ErpOrderItems> selectByMainId(String mainId);
}
